package com.swguild.rockpaperscissorscheatbot;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class Janken {

    public static void main(String[] args) {
        Random rnd = new Random();
        Scanner kb = new Scanner(System.in);

        int again = 1;
        System.out.println("Rock-Paper-Scissor time!");
        while (again == 1) {
            int player = 0, comp = 0, rounds = 0, i = 0, ties = 0, pw = 0, cw = 0, cheat;
            System.out.println("How many rounds would you like to play?\n");
            rounds = kb.nextInt();
            while (rounds < 1 || rounds > 10) {
                System.out.println("Select 1-10 rounds.");
                rounds = kb.nextInt();
            }
            while (i <= rounds - 1) {
                i++;
                System.out.println("\nWhich do you choose?\n"
                        + "Rock = 1\n"
                        + "Paper = 2\n"
                        + "Scissor = 3\n");
                player = kb.nextInt();
                comp = (rnd.nextInt(3) + 1);
//CheaterBot
                cheat = (rnd.nextInt(7));
                if (cheat == 0) {
                    player = 1;
                    comp = 2;
                    System.out.println("I Won! *cheater alert*");
                }
                if (player == comp) {
                    System.out.println("Tie");
                    ties++;
                } else if ((player == 1 && comp == 2) || (player == 2 && comp == 3) || (player == 3 && comp == 1)) {
                    if (cheat != 0) {
                        System.out.println("You Lost. lol");
                    }
                    cw++;
                } else if ((player == 2 && comp == 1) || (player == 3 && comp == 2) || (player == 1 && comp == 3)) {
                    System.out.println("You Won! Did you cheat?");
                    pw++;
                } else {
                    System.out.println("Try using 1, 2, or 3");
                    i--;
                }
            }
            System.out.println("\nTies: " + ties
                    + "\nYou Won: " + pw
                    + "\nI Won: " + cw);
            if (pw == cw) {
                System.out.println("Looks like we tied.");
            } else if (pw > cw) {
                System.out.println("You Won?! aww shucks");
            } else {
                System.out.println("You thought you could beat me? Ha!");
            }
            System.out.println("\nWould you like to play again?\n"
                    + "Yes = 1\n"
                    + "No = 2");
            again = kb.nextInt();
        }
        System.out.println("K THX BAI");
    }
}
